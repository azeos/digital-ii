module.exports = {
  purge: false,
  theme: {
    extend: {}
  },
  variants: {
      backgroundColor: ['responsive', 'hover', 'focus', 'disabled'],
  },
  plugins: [
    require('@tailwindcss/custom-forms')
  ]
}
