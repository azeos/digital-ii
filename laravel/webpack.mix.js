const mix = require('laravel-mix');
let path = require('path');

let publicPath = path.normalize('../public_html');
mix.setPublicPath(publicPath);

require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'js')
    .postCss('resources/css/app.css', 'css')
    .tailwind('./tailwind.config.js')
    .webpackConfig({
        watchOptions: {
            ignored: /node_modules/
        }
    });

if (mix.inProduction()) {
    mix.version()
        .purgeCss({
            extend: {
                content: [
                    path.join(__dirname, 'resources/js/app/components/theme.js'),
                ],
            },
        });
}
