<?php

namespace App;

use App\Http\Requests\StoreUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use DB;

class User extends Authenticatable
{
    use HasRoles, Notifiable, Sluggable;

    /**
     * Path de las imágenes del usuario.
     *
     * @var string
     */
    public $path = 'users/';

    /**
     * Archivos asociados al modelo.
     *
     * @var array
     */
    private $uploadFiles = [
        'avatar',
        'header',
        'products_image',
        'photos_image',
        'beauties_image',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'name', 'email', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'config'            => 'collection',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'unique' => true,
            ],
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Curso.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Fotos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * Beauties.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beauties()
    {
        return $this->hasMany(Beauty::class);
    }

    /**
     * Establece el nombre del usuario.
     *
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $name = '';

        // Si hay apellido
        if ($this->last_name) {
            $name .= $this->last_name;
        }

        // Si hay nombre
        if ($this->first_name) {
            $name .= $name ? ' ' . $this->first_name : $this->first_name;
        }

        $this->attributes['name'] = $name ?: $value;
    }

    /**
     * URL del avatar.
     *
     * @return string|null
     */
    public function getAvatarUrlAttribute()
    {
        if ($this->avatar) {
            return route('users.files.show', [$this, $this->avatar]);
        }

        return null;
    }

    /**
     * URL del header.
     *
     * @return string|null
     */
    public function getHeaderUrlAttribute()
    {
        if ($this->header) {
            return route('users.files.show', [$this, $this->header]);
        }

        return null;
    }

    /**
     * URL de la imagen de productos.
     *
     * @return string|null
     */
    public function getProductsImageUrlAttribute()
    {
        if ($this->products_image) {
            return route('users.files.show', [$this, $this->products_image]);
        }

        return null;
    }

    /**
     * URL de la imagen de fotos.
     *
     * @return string|null
     */
    public function getPhotosImageUrlAttribute()
    {
        if ($this->photos_image) {
            return route('users.files.show', [$this, $this->photos_image]);
        }

        return null;
    }

    /**
     * URL de la imagen de beauties.
     *
     * @return string|null
     */
    public function getBeautiesImageUrlAttribute()
    {
        if ($this->beauties_image) {
            return route('users.files.show', [$this, $this->beauties_image]);
        }

        return null;
    }

    /**
     * Crea o actualiza un usuario.
     *
     * @param StoreUser $request
     * @param User|null $user
     *
     * @return array
     * @throws \Throwable
     */
    public static function store(StoreUser $request, self $user = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($user) {
                $msg = 'editó';
                $update = true;
            } // Insert
            else {
                $user = new self;
                $msg = 'creó';
                $update = false;
            }

            // Campos
            $user->last_name = $request->input('last_name');
            $user->first_name = $request->input('first_name');
            $user->name = $update ? $user->name : null;
            $user->course_id = $request->input('course_id');

            // Imágenes
            $uploadedFiles = uploadFiles($user, $user->uploadFiles, $request, $update);

            // Commit
            $user->save();
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El perfil se ' . $msg . ' correctamente — <a class="text-blue-600 hover:underline" href="' . route('users.show', $user) . '">ver perfil</a>.',
                'data'    => $user,
            ];
        } catch (\Exception $e) {
            deleteFilesFromDb($user, $uploadedFiles);
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
