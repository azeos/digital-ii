<?php

namespace App\Policies;

use App\Beauty;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BeautyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any beauties.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // Si tiene permisos para ver beauties de cualquier usuario
        if ($user->hasPermissionTo('beauties.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('beauties.view') && ($user->id == request()->user->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the beauty.
     *
     * @param User   $user
     * @param Beauty $beauty
     *
     * @return mixed
     */
    public function view(User $user, Beauty $beauty)
    {
        // Si tiene permisos para ver beauties de cualquier usuario
        if ($user->hasPermissionTo('beauties.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('beauties.view') && ($user->id == $beauty->user_id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can create beauties.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        // Si tiene permisos para crear beauties en cualquier usuario
        if ($user->hasPermissionTo('beauties.createAny')) {
            return true;
        }

        // Si tiene permisos para crear y es su propio usuario
        if ($user->hasPermissionTo('beauties.create') && ($user->id == request()->user->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the beauty.
     *
     * @param User   $user
     * @param Beauty $beauty
     *
     * @return mixed
     */
    public function update(User $user, Beauty $beauty)
    {
        // Si tiene permisos para editar beauties de cualquier usuario
        if ($user->hasPermissionTo('beauties.updateAny')) {
            return true;
        }

        // Si tiene permisos para editar y es su propio usuario
        if ($user->hasPermissionTo('beauties.update') && ($user->id == $beauty->user_id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the beauty.
     *
     * @param User   $user
     * @param Beauty $beauty
     *
     * @return mixed
     */
    public function delete(User $user, Beauty $beauty)
    {
        // Si tiene permisos para eliminar beauties de cualquier usuario
        if ($user->hasPermissionTo('beauties.deleteAny')) {
            return true;
        }

        // Si tiene permisos para eliminar y es su propio usuario
        if ($user->hasPermissionTo('beauties.delete') && ($user->id == $beauty->user_id)) {
            return true;
        }
    }
}
