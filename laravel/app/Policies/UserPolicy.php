<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any users.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param User $user
     * @param User $model
     *
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        // Si tiene permisos para ver cualquier usuario
        if ($user->hasPermissionTo('users.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('users.view') && ($user->id == $model->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  User $user
     * @param  User $model
     *
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        // Si tiene permisos para editar cualquier usuario
        if ($user->hasPermissionTo('users.updateAny')) {
            return true;
        }

        // Si tiene permisos para editar y es su propio usuario
        if ($user->hasPermissionTo('users.update') && ($user->id == $model->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param User  $user
     * @param  User $model
     *
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can restore the user.
     *
     * @param  User $user
     * @param  User $model
     *
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the user.
     *
     * @param  User  $user
     * @param  User  $model
     *
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        //
    }
}
