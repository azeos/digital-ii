<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any courses.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // Si tiene permisos para ver cualquier comisión
        if ($user->hasPermissionTo('courses.viewAny')) {
            return true;
        }
    }
}
