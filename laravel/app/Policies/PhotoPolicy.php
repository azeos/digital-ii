<?php

namespace App\Policies;

use App\Photo;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PhotoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any photos.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // Si tiene permisos para ver fotos de cualquier usuario
        if ($user->hasPermissionTo('photos.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('photos.view') && ($user->id == request()->user->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the photo.
     *
     * @param User  $user
     * @param Photo $photo
     *
     * @return mixed
     */
    public function view(User $user, Photo $photo)
    {
        // Si tiene permisos para ver fotos de cualquier usuario
        if ($user->hasPermissionTo('photos.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('photos.view') && ($user->id == $photo->user_id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can create photos.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        // Si tiene permisos para crear fotos en cualquier usuario
        if ($user->hasPermissionTo('photos.createAny')) {
            return true;
        }

        // Si tiene permisos para crear y es su propio usuario
        if ($user->hasPermissionTo('photos.create') && ($user->id == request()->user->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the photo.
     *
     * @param User  $user
     * @param Photo $photo
     *
     * @return mixed
     */
    public function update(User $user, Photo $photo)
    {
        // Si tiene permisos para editar fotos de cualquier usuario
        if ($user->hasPermissionTo('photos.updateAny')) {
            return true;
        }

        // Si tiene permisos para editar y es su propio usuario
        if ($user->hasPermissionTo('photos.update') && ($user->id == $photo->user_id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the photo.
     *
     * @param User  $user
     * @param Photo $photo
     *
     * @return mixed
     */
    public function delete(User $user, Photo $photo)
    {
        // Si tiene permisos para eliminar fotos de cualquier usuario
        if ($user->hasPermissionTo('photos.deleteAny')) {
            return true;
        }

        // Si tiene permisos para eliminar y es su propio usuario
        if ($user->hasPermissionTo('photos.delete') && ($user->id == $photo->user_id)) {
            return true;
        }
    }
}
