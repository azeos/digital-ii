<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any products.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        // Si tiene permisos para ver productos de cualquier usuario
        if ($user->hasPermissionTo('products.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('products.view') && ($user->id == request()->user->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the product.
     *
     * @param User    $user
     * @param Product $product
     *
     * @return mixed
     */
    public function view(User $user, Product $product)
    {
        // Si tiene permisos para ver productos de cualquier usuario
        if ($user->hasPermissionTo('products.viewAny')) {
            return true;
        }

        // Si tiene permisos para ver y es su propio usuario
        if ($user->hasPermissionTo('products.view') && ($user->id == $product->user_id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can create products.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        // Si tiene permisos para crear productos en cualquier usuario
        if ($user->hasPermissionTo('products.createAny')) {
            return true;
        }

        // Si tiene permisos para crear y es su propio usuario
        if ($user->hasPermissionTo('products.create') && ($user->id == request()->user->id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param User    $user
     * @param Product $product
     *
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        // Si tiene permisos para editar productos de cualquier usuario
        if ($user->hasPermissionTo('products.updateAny')) {
            return true;
        }

        // Si tiene permisos para editar y es su propio usuario
        if ($user->hasPermissionTo('products.update') && ($user->id == $product->user_id)) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param User    $user
     * @param Product $product
     *
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        // Si tiene permisos para eliminar productos de cualquier usuario
        if ($user->hasPermissionTo('products.deleteAny')) {
            return true;
        }

        // Si tiene permisos para eliminar y es su propio usuario
        if ($user->hasPermissionTo('products.delete') && ($user->id == $product->user_id)) {
            return true;
        }
    }
}
