<?php

namespace App;

use App\Http\Requests\SortProduct;
use App\Http\Requests\StoreProduct;
use Cviebrock\EloquentSluggable\Sluggable;
use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Sluggable;

    /**
     * Path de las imágenes de las fotos.
     *
     * @var string
     */
    public $path = 'products/';

    /**
     * Archivos asociados al modelo.
     *
     * @var array
     */
    private $uploadFiles = ['thumbnail'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Estudiante.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Antes/después del producto.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function picture()
    {
        return $this->morphOne(Picture::class, 'pictureable');
    }

    /**
     * Antes/después del producto, esta relación se usa para guardar las imágenes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function pictures()
    {
        return $this->morphMany(Picture::class, 'pictureable');
    }

    /**
     * URL de la miniatura.
     *
     * @return string|null
     */
    public function getThumbnailUrlAttribute()
    {
        if ($this->thumbnail) {
            return route('products.files.show', [$this, $this->thumbnail]);
        }

        return null;
    }

    /**
     * Crea o actualiza un producto.
     *
     * @param StoreProduct $request
     * @param User         $user
     * @param Product|null $product
     *
     * @return array
     * @throws \Throwable
     */
    public static function store(StoreProduct $request, User $user, self $product = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($product) {
                $msg = 'editó';
                $update = true;
            } // Insert
            else {
                $product = new self;
                $msg = 'creó';
                $update = false;
            }

            // Campos
            $product->name = $request->input('name');
            $product->price = $request->input('price');

            // Miniatura
            $uploadedFiles = uploadFiles($product, $product->uploadFiles, $request, $update);

            $user->products()->save($product);

            // Before & after
            if (! Picture::store($request, $product, $product->picture)) {
                throw new \Exception('No se pudo subir la imagen');
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El producto se ' . $msg . ' correctamente — <a class="text-blue-600 hover:underline" href="' . route('products.index', $user) . '">ver productos</a>.',
                'data'    => $product,
            ];
        } catch (\Exception $e) {
            deleteFilesFromDb($product, $uploadedFiles);
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Reordena los productos.
     *
     * @param SortProduct $request
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public static function order(SortProduct $request)
    {
        DB::beginTransaction();
        try {
            $products = $request->input('products', []);

            if (count($products)) {
                foreach ($products as $i => $id) {
                    $product = self::findOrFail($id);
                    $product->sort = $i;
                    $product->save();
                }
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'Los productos se ordenaron correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Elimina un producto.
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina el producto
            $this->delete();

            // Elimina los archivos asociados
            deleteFilesFromDb($this, $this->uploadFiles);

            // Elimina las imágenes asociadas
            $this->picture->eliminar();

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El producto se eliminó correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
