<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

if (! function_exists('uploadFiles')) {
    /**
     * Sube archivos y borra viejos.
     *
     * @param                                        $obj
     * @param array                                  $fields
     * @param Illuminate\Foundation\Http\FormRequest $request
     * @param bool|null                              $update
     *
     * @return array
     * @throws Exception
     */
    function uploadFiles($obj, array $fields, $request, bool $update = null)
    {
        $uploadedFiles = [];

        foreach ($fields as $fieldName) {
            // Archivo
            $delete = $request->input($fieldName . '_delete') ? true : false;
            $file = $request->file($fieldName);

            if ($file) {
                // Si ya hay un archivo cargado, hay que borrar el anterior
                $delete = $obj->$fieldName ?? false;

                // Extensión del archivo
                $extension = Str::lower($file->getClientOriginalExtension());

                // Resta la cantidad de caracteres de la extensión al largo del nombre
                $length = 38 - strlen($extension);

                // Chequea que el nombre no se repita
                do {
                    $name = Str::random($length) . '.' . $extension;
                } while (Storage::exists($obj->path . $name));

                // Si se subió correctamente el archivo
                if (Storage::putFileAs($obj->path, $file, $name)) {
                    $obj->$fieldName = $name;
                    $uploadedFiles[] = $fieldName;
                } else {
                    $error = 'Se produjo un error al subir el archivo "' . $file->getClientOriginalName() . '".';
                    throw new \Exception($error);
                }
            } // Sí es un Update y se borró el archivo
            elseif ($update && $delete) {
                $delete = $obj->$fieldName;
                $obj->$fieldName = null;
            }

            // Elimina el archivo
            if ($delete) {
                deleteFile($delete, $obj->path);
            }
        }

        return $uploadedFiles;
    }
}

if (! function_exists('deleteFilesFromDb')) {
    /**
     * Borra archivos que corresponden a campos de la DB.
     *
     * @param object       $obj
     * @param string|array $fieldNames
     *
     * @throws Exception
     */
    function deleteFilesFromDb($obj, $fieldNames)
    {
        $fieldNames = is_array($fieldNames) ? $fieldNames : [$fieldNames];
        foreach ($fieldNames as $fieldName) {
            if ($obj->$fieldName) {
                deleteFile($obj->$fieldName, $obj->path);
            }
        }
    }
}

if (! function_exists('deleteFile')) {
    /**
     * Elimina un archivo.
     *
     * @param string $fileName nombre del archivo.
     * @param string $path ruta del archivo.
     *
     * @return bool
     * @throws Exception
     */
    function deleteFile(string $fileName, string $path)
    {
        try {
            Storage::delete($path . $fileName);

            return true;
        } catch (\Exception $e) {
            $error = 'No se pudo eliminar el archivo "' . $fileName . '".';
            throw new \Exception($error);

            return false;
        }
    }
}
