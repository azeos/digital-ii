<?php

namespace App;

use App\Http\Requests\SortPhoto;
use App\Http\Requests\StorePhoto;
use DB;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * Path de las imágenes de las fotos.
     *
     * @var string
     */
    public $path = 'photos/';

    /**
     * Archivos asociados al modelo.
     *
     * @var array
     */
    private $uploadFiles = ['thumbnail'];

    /**
     * Estudiante.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Antes/después de la foto.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function picture()
    {
        return $this->morphOne(Picture::class, 'pictureable');
    }

    /**
     * Antes/después de la foto, esta relación se usa para guardar las imágenes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function pictures()
    {
        return $this->morphMany(Picture::class, 'pictureable');
    }

    /**
     * URL de la miniatura.
     *
     * @return string|null
     */
    public function getThumbnailUrlAttribute()
    {
        if ($this->thumbnail) {
            return route('photos.files.show', [$this, $this->thumbnail]);
        }

        return null;
    }

    /**
     * Crea o actualiza una foto.
     *
     * @param StorePhoto $request
     * @param User       $user
     * @param Photo|null $photo
     *
     * @return array
     * @throws \Throwable
     */
    public static function store(StorePhoto $request, User $user, self $photo = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($photo) {
                $msg = 'editó';
                $update = true;
            } // Insert
            else {
                $photo = new self;
                $msg = 'creó';
                $update = false;
            }

            // Miniatura
            $uploadedFiles = uploadFiles($photo, $photo->uploadFiles, $request, $update);

            $user->photos()->save($photo);

            // Before & after
            if (! Picture::store($request, $photo, $photo->picture)) {
                throw new \Exception('No se pudo subir la imagen');
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'La foto se ' . $msg . ' correctamente — <a class="text-blue-600 hover:underline" href="' . route('photos.index', $user) . '">ver fotos</a>.',
                'data'    => $photo,
            ];
        } catch (\Exception $e) {
            deleteFilesFromDb($photo, $uploadedFiles);
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Reordena las fotos.
     *
     * @param SortPhoto $request
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public static function order(SortPhoto $request)
    {
        DB::beginTransaction();
        try {
            $photos = $request->input('photos', []);

            if (count($photos)) {
                foreach ($photos as $i => $id) {
                    $photo = self::findOrFail($id);
                    $photo->sort = $i;
                    $photo->save();
                }
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'Las fotos se ordenaron correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Elimina una foto.
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina la foto
            $this->delete();

            // Elimina los archivos asociados
            deleteFilesFromDb($this, $this->uploadFiles);

            // Elimina las imágenes asociadas
            $this->picture->eliminar();

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'La foto se eliminó correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
