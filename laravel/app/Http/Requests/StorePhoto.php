<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePhoto extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thumbnail' => ['image', 'max:150', 'dimensions:width=322,height=322'],
            'before'    => ['image', 'max:600', 'dimensions:max_width=1080,max_height=1080'],
            'after'     => ['image', 'max:600', 'dimensions:max_width=1080,max_height=1080'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'thumbnail' => 'miniatura',
            'before'    => 'original',
            'after'     => 'final',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'thumbnail.dimensions' => 'La definición de la imagen :attribute debe ser de :width x :height px.',
            'before.dimensions'    => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
            'after.dimensions'     => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
        ];
    }
}
