<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name'      => ['required', 'max:255'],
            'first_name'     => ['required', 'max:255'],
            'course_id'      => ['required', 'exists:courses,id'],
            'avatar'         => ['image', 'max:100', 'dimensions:width=200,height=200'],
            'header'         => ['image', 'max:400', 'dimensions:width=1920,height=640'],
            'products_image' => ['image', 'max:200', 'dimensions:width=768,height=432'],
            'photos_image'   => ['image', 'max:200', 'dimensions:width=768,height=432'],
            'beauties_image' => ['image', 'max:200', 'dimensions:width=768,height=432'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'course_id'      => 'comisión',
            'products_image' => 'productos',
            'photos_image'   => 'fotos',
            'beauties_image' => 'beauties',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'header.dimensions'         => 'La definición de la imagen :attribute debe ser de :width x :height px.',
            'avatar.dimensions'         => 'La definición de la imagen :attribute debe ser de :width x :height px.',
            'products_image.dimensions' => 'La definición de la imagen :attribute debe ser de :width x :height px.',
            'photos_image.dimensions'   => 'La definición de la imagen :attribute debe ser de :width x :height px.',
            'beauties_image.dimensions' => 'La definición de la imagen :attribute debe ser de :width x :height px.',
        ];
    }
}
