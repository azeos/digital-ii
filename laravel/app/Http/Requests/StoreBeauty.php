<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBeauty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thumbnail' => ['image', 'max:200', 'dimensions:width=768,height=768'],
            'before'    => ['image', 'max:700', 'dimensions:max_width=2000,max_height=2000'],
            'lightroom' => ['image', 'max:700', 'dimensions:max_width=2000,max_height=2000'],
            'photoshop' => ['image', 'max:700', 'dimensions:max_width=2000,max_height=2000'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'thumbnail' => 'miniatura',
            'before'    => 'original',
            'lightroom' => 'Lightroom',
            'photoshop' => 'Photoshop',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'thumbnail.dimensions' => 'La definición de la imagen :attribute debe ser de :width x :height px.',
            'before.dimensions'    => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
            'lightroom.dimensions' => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
            'photoshop.dimensions' => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
        ];
    }
}
