<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required', 'max:255'],
            'price'     => ['required', 'max:255'],
            'thumbnail' => ['image', 'max:250', 'dimensions:max_width=827,max_height=465,min_width=296,min_height=167'],
            'before'    => ['image', 'max:600', 'dimensions:max_width=1080,max_height=1080'],
            'after'     => ['image', 'max:1500', 'dimensions:max_width=1080,max_height=1080'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'      => 'nombre',
            'price'     => 'precio',
            'thumbnail' => 'miniatura',
            'before'    => 'original',
            'after'     => 'final',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'thumbnail.dimensions' => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px, no debe ser inferior a :min_width x :min_height px y debe tener una relación de aspecto 16:9.',
            'before.dimensions'    => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
            'after.dimensions'     => 'La definición de la imagen :attribute no debe superar los :max_width x :max_height px.',
        ];
    }
}
