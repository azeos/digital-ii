<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\StoreUser;
use App\Http\Resources\UserResource;
use App\User;

class UserController extends Controller
{
    /**
     * Policy.
     */
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra el home del usuario.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $this->data['user'] = new UserResource($user->load('course'));
        $this->data['title'] = $user->name . ' | ' . config('app.name');

        return view('users.show', $this->data);
    }

    /**
     * Devuelve la información para editar un usuario.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $this->data['data'] = json_encode([
            'user'    => new UserResource($user),
            'courses' => Course::orderBy('name')->get(['name', 'id'])->transform(function ($item) {
                return [
                    'value' => $item->id,
                    'text'  => $item->name,
                ];
            }),
            'route'   => route('users.update', $user),
            'method'  => 'patch',
        ]);
        $this->data['profile'] = json_encode([
            'name'   => $user->name,
            'avatar' => $user->avatarUrl,
            'url'    => auth()->user()->can('view', $user) ? route('users.show', $user) : null,
        ]);
        $this->data['section'] = 'users';
        $this->data['routes'] = json_encode([
            'profile'  => route('users.edit', $user),
            'products' => route('products.create', $user),
            'photos'   => route('photos.create', $user),
            'beauties' => route('beauties.create', $user),
        ]);
        $this->data['title'] = 'Editar perfil | ' . config('app.name');

        return view('settings.show', $this->data);
    }

    /**
     * Actualiza un usuario.
     *
     * @param StoreUser $request
     * @param User      $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(StoreUser $request, User $user)
    {
        $response = User::store($request, $user);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve los datos del usuario para actualizar la vista
        return response()->json([
            'message' => $response['message'],
            'data'    => [
                'user'    => new UserResource($response['data']),
                'courses' => Course::orderBy('name')->get(['name', 'id'])->transform(function ($item) {
                    return [
                        'value' => $item->id,
                        'text'  => $item->name,
                    ];
                }),
                'route'   => route('users.update', $response['data']),
                'method'  => 'patch',
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
