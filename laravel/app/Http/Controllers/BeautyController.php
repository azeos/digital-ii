<?php

namespace App\Http\Controllers;

use App\Http\Requests\SortBeauty;
use App\Http\Requests\StoreBeauty;
use App\Http\Resources\BeautyResource;
use App\Http\Resources\UserResource;
use App\Beauty;
use App\User;

class BeautyController extends Controller
{
    /**
     * Policy.
     */
    public function __construct()
    {
        $this->authorizeResource(Beauty::class, 'beauty');
    }

    /**
     * Muestra el listado de beauties.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {
        $this->data['user'] = new UserResource($user->load('course'));
        $beauties = $user->beauties()->orderBy('sort')->get();
        $this->data['beauties'] = BeautyResource::collection($beauties);
        $this->data['title'] = 'Beauties | ' . config('app.name');

        return view('beauties.index', $this->data);
    }

    /**
     * Devuelve la información para crear un nuevo beauty.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {
        // Si es el modal de creación
        if (request()->ajax()) {
            $beauty = new Beauty();
            $this->data['beauty'] = new BeautyResource($beauty);
            $this->data['route'] = route('beauties.store', $user);
            $this->data['method'] = 'post';
            $this->data['title'] = 'Nuevo beauty';

            return response()->json($this->data);
        }

        $beauties = $user->beauties()->orderBy('sort')->get();
        $this->data['data'] = json_encode([
            'beauties'  => BeautyResource::collection($beauties),
            'createUrl' => route('beauties.create', $user),
            'route'     => route('beauties.sort', $user),
        ]);
        $this->data['profile'] = json_encode([
            'name'   => $user->name,
            'avatar' => $user->avatarUrl,
            'url'    => auth()->user()->can('view', $user) ? route('users.show', $user) : null,
        ]);
        $this->data['section'] = 'beauties';
        $this->data['routes'] = json_encode([
            'profile'  => route('users.edit', $user),
            'products' => route('products.create', $user),
            'photos'   => route('photos.create', $user),
            'beauties' => route('beauties.create', $user),
        ]);
        $this->data['title'] = 'Editar beauties | ' . config('app.name');

        return view('settings.show', $this->data);
    }

    /**
     * Guarda un nuevo beauty.
     *
     * @param StoreBeauty $request
     * @param User        $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\Throwable
     */
    public function store(StoreBeauty $request, User $user)
    {
        $response = Beauty::store($request, $user);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas los beauties para actualizar la vista
        $beauties = $user->beauties()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => BeautyResource::collection($beauties),
        ]);
    }

    /**
     * Devuelve la información para editar un beauty.
     *
     * @param User   $user
     * @param Beauty $beauty
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(User $user, Beauty $beauty)
    {
        $this->data['beauty'] = new BeautyResource($beauty);
        $this->data['route'] = route('beauties.update', [$user, $beauty]);
        $this->data['method'] = 'patch';
        $this->data['title'] = 'Editar beauty';

        return response()->json($this->data);
    }

    /**
     * Actualiza un beauty.
     *
     * @param StoreBeauty $request
     * @param User        $user
     * @param Beauty      $beauty
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\Throwable
     */
    public function update(StoreBeauty $request, User $user, Beauty $beauty)
    {
        $response = Beauty::store($request, $user, $beauty);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas los beauties para actualizar la vista
        $beauties = $user->beauties()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => BeautyResource::collection($beauties),
        ]);
    }

    /**
     * Elimina un beauty.
     *
     * @param User   $user
     * @param Beauty $beauty
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(User $user, Beauty $beauty)
    {
        $response = $beauty->eliminar();

        // Sí se produjo un error al eliminar
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas los beauties para actualizar la vista
        $beauties = $user->beauties()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => BeautyResource::collection($beauties),
        ]);
    }

    /**
     * Ordena los beauties de un usuario.
     *
     * @param SortBeauty $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function sort(SortBeauty $request)
    {
        $this->authorize('create', Beauty::class);

        $response = Beauty::order($request);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        return response()->json([
            'message' => $response['message'],
        ]);
    }
}
