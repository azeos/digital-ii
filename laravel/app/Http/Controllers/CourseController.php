<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Resources\CourseResource;

class CourseController extends Controller
{
    /**
     * Policy.
     */
    public function __construct()
    {
        $this->authorizeResource(Course::class, 'course');
    }

    /**
     * Muestra el listado de comisiones con sus alumnos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::published()->has('users')->with(['users' => function ($query) {
            $query->orderBy('name');
        }])->orderByDesc('name')->get(['id', 'name', 'slug']);
        $this->data['courses'] = CourseResource::collection($courses);
        $this->data['title'] =  'Comisiones | ' . config('app.name');

        return view('courses.index', $this->data);
    }
}
