<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    protected function redirectTo()
    {
        // Si es un estudiante, lo redirecciona a su perfil
        if (auth()->user()->hasRole('student')) {
            return route('users.show', auth()->user());
        }

        return route('index');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param $provider
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->with(['prompt' => 'select_account'])->redirect();
    }

    /**
     * Obtain the user information from the provider.
     *
     * @param $provider
     *
     * @return void
     */
    public function handleProviderCallback($provider)
    {
        try {
            // Usuario OAuth
            $OAuthUser = Socialite::driver($provider)->user();

            // Busca al usuario en la DB
            $user = User::where('provider', $provider)->where('provider_id', $OAuthUser->getId())->first();

            // Si no está registrado lo registra como estudiante
            if (! $user) {
                $user = new User();
                $user->provider = $provider;
                $user->provider_id = $OAuthUser->getId();
                $user->last_name = $OAuthUser->user['family_name'] ?? null;
                $user->first_name = $OAuthUser->user['given_name'] ?? null;
                $user->name = $OAuthUser->getName();
                $user->email = $OAuthUser->getEmail();
                $user->save();
                $user->assignRole('student');
            }

            // Loguea al usuario
            Auth::login($user, true);

            return redirect($this->redirectTo());
        } catch (\Exception $e) {
            return redirect('login');
        }
    }
}
