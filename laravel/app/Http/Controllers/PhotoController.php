<?php

namespace App\Http\Controllers;

use App\Http\Requests\SortPhoto;
use App\Http\Requests\StorePhoto;
use App\Http\Resources\PhotoResource;
use App\Http\Resources\UserResource;
use App\Photo;
use App\User;

class PhotoController extends Controller
{
    /**
     * Policy.
     */
    public function __construct()
    {
        $this->authorizeResource(Photo::class, 'photo');
    }

    /**
     * Muestra el listado de fotos.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {
        $this->data['user'] = new UserResource($user->load('course'));
        $photos = $user->photos()->with('picture')->orderBy('sort')->get();
        $this->data['photos'] = PhotoResource::collection($photos);
        $this->data['title'] =  'Fotos | ' . config('app.name');

        return view('photos.index', $this->data);
    }

    /**
     * Devuelve la información para crear una nueva foto.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {
        // Si es el modal de creación
        if (request()->ajax()) {
            $photo = new Photo();
            $this->data['photo'] = new PhotoResource($photo);
            $this->data['route'] = route('photos.store', $user);
            $this->data['method'] = 'post';
            $this->data['title'] = 'Nueva foto';

            return response()->json($this->data);
        }

        $photos = $user->photos()->orderBy('sort')->get();
        $this->data['data'] = json_encode([
            'photos'    => PhotoResource::collection($photos),
            'createUrl' => route('photos.create', $user),
            'route'     => route('photos.sort', $user),
        ]);
        $this->data['profile'] = json_encode([
            'name'   => $user->name,
            'avatar' => $user->avatarUrl,
            'url'    => auth()->user()->can('view', $user) ? route('users.show', $user) : null,
        ]);
        $this->data['section'] = 'photos';
        $this->data['routes'] = json_encode([
            'profile'  => route('users.edit', $user),
            'products' => route('products.create', $user),
            'photos'   => route('photos.create', $user),
            'beauties' => route('beauties.create', $user),
        ]);
        $this->data['title'] = 'Editar fotos | ' . config('app.name');

        return view('settings.show', $this->data);
    }

    /**
     * Guarda una nueva foto.
     *
     * @param StorePhoto $request
     * @param User       $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\Throwable
     */
    public function store(StorePhoto $request, User $user)
    {
        $response = Photo::store($request, $user);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas las fotos para actualizar la vista
        $photos = $user->photos()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => PhotoResource::collection($photos),
        ]);
    }

    /**
     * Devuelve la información para editar una foto.
     *
     * @param User  $user
     * @param Photo $photo
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(User $user, Photo $photo)
    {
        $photo->load('picture');
        $this->data['photo'] = new PhotoResource($photo);
        $this->data['route'] = route('photos.update', [$user, $photo]);
        $this->data['method'] = 'patch';
        $this->data['title'] = 'Editar foto';

        return response()->json($this->data);
    }

    /**
     * Actualiza una foto.
     *
     * @param StorePhoto $request
     * @param User       $user
     * @param Photo      $photo
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\Throwable
     */
    public function update(StorePhoto $request, User $user, Photo $photo)
    {
        $response = Photo::store($request, $user, $photo);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas las fotos para actualizar la vista
        $photos = $user->photos()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => PhotoResource::collection($photos),
        ]);
    }

    /**
     * Elimina una foto.
     *
     * @param User  $user
     * @param Photo $photo
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(User $user, Photo $photo)
    {
        $response = $photo->eliminar();

        // Sí se produjo un error al eliminar
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas las fotos para actualizar la vista
        $photos = $user->photos()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => PhotoResource::collection($photos),
        ]);
    }

    /**
     * Ordena las fotos de un usuario.
     *
     * @param SortPhoto $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function sort(SortPhoto $request)
    {
        $this->authorize('create', Photo::class);

        $response = Photo::order($request);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        return response()->json([
            'message' => $response['message'],
        ]);
    }
}
