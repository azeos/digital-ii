<?php

namespace App\Http\Controllers;

use App\Http\Requests\SortProduct;
use App\Http\Requests\StoreProduct;
use App\Http\Resources\ProductResource;
use App\Http\Resources\UserResource;
use App\Product;
use App\User;

class ProductController extends Controller
{
    /**
     * Policy.
     */
    public function __construct()
    {
        $this->authorizeResource(Product::class, 'product');
    }

    /**
     * Muestra el listado de productos.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {
        $this->data['user'] = new UserResource($user->load('course'));
        $products = $user->products()->with('picture')->orderBy('sort')->get();
        $this->data['products'] = ProductResource::collection($products);
        $this->data['title'] =  'Productos | ' . config('app.name');

        return view('products.index', $this->data);
    }

    /**
     * Devuelve la información para crear un nuevo producto.
     *
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {
        // Si es el modal de creación
        if (request()->ajax()) {
            $product = new Product();
            $this->data['product'] = new ProductResource($product);
            $this->data['route'] = route('products.store', $user);
            $this->data['method'] = 'post';
            $this->data['title'] = 'Nuevo producto';

            return response()->json($this->data);
        }

        $products = $user->products()->orderBy('sort')->get();
        $this->data['data'] = json_encode([
            'products'    => ProductResource::collection($products),
            'createUrl' => route('products.create', $user),
            'route'     => route('products.sort', $user),
        ]);
        $this->data['profile'] = json_encode([
            'name'   => $user->name,
            'avatar' => $user->avatarUrl,
            'url'    => auth()->user()->can('view', $user) ? route('users.show', $user) : null,
        ]);
        $this->data['section'] = 'products';
        $this->data['routes'] = json_encode([
            'profile'  => route('users.edit', $user),
            'products' => route('products.create', $user),
            'photos'   => route('photos.create', $user),
            'beauties' => route('beauties.create', $user),
        ]);
        $this->data['title'] = 'Editar productos | ' . config('app.name');

        return view('settings.show', $this->data);
    }

    /**
     * Guarda un nuevo producto.
     *
     * @param StoreProduct $request
     * @param User         $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\Throwable
     */
    public function store(StoreProduct $request, User $user)
    {
        $response = Product::store($request, $user);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas los productos para actualizar la vista
        $products = $user->products()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => ProductResource::collection($products),
        ]);
    }

    /**
     * Devuelve la información para editar un producto.
     *
     * @param User    $user
     * @param Product $product
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(User $user, Product $product)
    {
        $product->load('picture');
        $this->data['product'] = new ProductResource($product);
        $this->data['route'] = route('products.update', [$user, $product]);
        $this->data['method'] = 'patch';
        $this->data['title'] = 'Editar producto';

        return response()->json($this->data);
    }

    /**
     * Actualiza un producto.
     *
     * @param StoreProduct $request
     * @param User         $user
     * @param Product      $product
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception|\Throwable
     */
    public function update(StoreProduct $request, User $user, Product $product)
    {
        $response = Product::store($request, $user, $product);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas los productos para actualizar la vista
        $products = $user->products()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => ProductResource::collection($products),
        ]);
    }

    /**
     * Elimina un producto.
     *
     * @param User    $user
     * @param Product $product
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function destroy(User $user, Product $product)
    {
        $response = $product->eliminar();

        // Sí se produjo un error al eliminar
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        // Devuelve todas los productos para actualizar la vista
        $products = $user->products()->orderBy('sort')->get();

        return response()->json([
            'message' => $response['message'],
            'data'    => ProductResource::collection($products),
        ]);
    }

    /**
     * Ordena los productos de un usuario.
     *
     * @param SortProduct $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function sort(SortProduct $request)
    {
        $this->authorize('create', Product::class);

        $response = Product::order($request);

        // Sí se produjo un error en la carga
        if ($response['status'] === 'error') {
            return response()->json($response['message'], 422);
        }

        return response()->json([
            'message' => $response['message'],
        ]);
    }
}
