<?php

namespace App\Http\Controllers;

use Storage;

class FileController extends Controller
{
    /**
     * Muestra una imagen.
     *
     * @param object $model User, Product, Photo, Beauty.
     * @param string $file  nombre del archivo a mostrar.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function show(object $model, string $file)
    {
        // Si existe el archivo
        if (Storage::exists($model->path . $file)) {
            $path = Storage::path($model->path . $file);
            $type = Storage::mimeType($model->path . $file);
            $headers = ['Content-Type', $type];

            return response()->file($path, $headers);
        }

        abort(404);
    }
}
