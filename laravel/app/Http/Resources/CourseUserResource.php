<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'      => $this->name,
            'slug'      => $this->slug,
            'avatarUrl' => $this->avatarUrl,
            'headerUrl' => $this->headerUrl,
            'showUrl'   => auth()->user()->can('view', $this->resource) ? route('users.show', $this) : null,
        ];
    }
}
