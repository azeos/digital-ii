<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'course_id'        => $this->course_id,
            'last_name'        => $this->last_name,
            'first_name'       => $this->first_name,
            'name'             => $this->name,
            'slug'             => $this->slug,
            'email'            => $this->email,
            'avatarUrl'        => $this->avatarUrl,
            'headerUrl'        => $this->headerUrl,
            'productsImageUrl' => $this->productsImageUrl,
            'photosImageUrl'   => $this->photosImageUrl,
            'beautiesImageUrl' => $this->beautiesImageUrl,
            'config'           => $this->config,
            'editUrl'          => auth()->user()->can('update', $this->resource) ? route('users.edit', $this) : null,
            'showUrl'          => auth()->user()->can('view', $this->resource) ? route('users.show', $this) : null,
            'routes'           => [
                'products.index' => route('products.index', $this),
                'photos.index'   => route('photos.index', $this),
                'beauties.index' => route('beauties.index', $this),
            ],
            'course'           => $this->whenLoaded('course', function () {
                return [
                    'name' => $this->course->name,
                    'slug' => $this->course->slug,
                    'show' => route('index') . '#' . $this->course->slug,
                ];
            }),
        ];
    }
}
