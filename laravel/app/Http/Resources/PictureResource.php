<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PictureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'beforeUrl' => $this->beforeUrl,
            'afterUrl'  => $this->afterUrl,
            'sort'      => $this->sort,
        ];
    }
}
