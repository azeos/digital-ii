<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BeautyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'thumbnailUrl' => $this->thumbnailUrl,
            'beforeUrl'    => $this->beforeUrl,
            'lightroomUrl' => $this->lightroomUrl,
            'photoshopUrl' => $this->photoshopUrl,
            'sort'         => $this->sort,
            'showUrl'      => ($this->id && auth()->user()->can('view', $this->resource)) ? route('beauties.show', [$this->user, $this]) : null,
            'editUrl'      => ($this->id && auth()->user()->can('update', $this->resource)) ? route('beauties.edit', [$this->user, $this]) : null,
        ];
    }
}
