<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'thumbnailUrl' => $this->thumbnailUrl,
            'sort'         => $this->sort,
            'showUrl'      => ($this->id && auth()->user()->can('view', $this->resource)) ? route('photos.show', [$this->user, $this]) : null,
            'editUrl'      => ($this->id && auth()->user()->can('update', $this->resource)) ? route('photos.edit', [$this->user, $this]) : null,
            'picture'      => $this->whenLoaded('picture', function () {
                return new PictureResource($this->picture);
            }),
        ];
    }
}
