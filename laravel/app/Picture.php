<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    /**
     * Path de las imágenes de las fotos.
     *
     * @var string
     */
    public $path = 'pictures/';

    /**
     * Archivos asociados al modelo.
     *
     * @var array
     */
    private $uploadFiles = ['before', 'after'];

    /**
     * Get the owning commentable model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function pictureable()
    {
        return $this->morphTo();
    }

    /**
     * URL de la imagen original.
     *
     * @return string|null
     */
    public function getBeforeUrlAttribute()
    {
        if ($this->before) {
            return route('pictures.files.show', [$this, $this->before]);
        }

        return null;
    }

    /**
     * URL de la imagen final.
     *
     * @return string|null
     */
    public function getAfterUrlAttribute()
    {
        if ($this->after) {
            return route('pictures.files.show', [$this, $this->after]);
        }

        return null;
    }

    /**
     * Crea o actualiza las imágenes asociadas a un modelo.
     *
     * @param              $request
     * @param              $owner
     * @param Picture|null $picture
     *
     * @return Picture|false|null
     * @throws \Exception|\Throwable
     */
    public static function store($request, $owner, self $picture = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($picture) {
                $update = true;
            } // Insert
            else {
                $picture = new self;
                $update = false;
            }

            // Imágenes
            $uploadedFiles = uploadFiles($picture, $picture->uploadFiles, $request, $update);

            // Commit
            $owner->pictures()->save($picture);
            DB::commit();

            return $picture;
        } catch (\Exception $e) {
            deleteFilesFromDb($picture, $uploadedFiles);
            DB::rollback();
            dd($e);

            return false;
        }
    }

    /**
     * Elimina una imagen.
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina la imagen
            $this->delete();

            // Elimina los archivos
            deleteFilesFromDb($this, $this->uploadFiles);

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'La imagen se eliminó correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
