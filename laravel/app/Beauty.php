<?php

namespace App;

use App\Http\Requests\StoreBeauty;
use DB;
use Illuminate\Database\Eloquent\Model;

class Beauty extends Model
{
    /**
     * Path de las imágenes de los beauties.
     *
     * @var string
     */
    public $path = 'beauties/';

    /**
     * Archivos asociados al modelo.
     *
     * @var array
     */
    private $uploadFiles = ['thumbnail', 'before', 'lightroom', 'photoshop'];

    /**
     * Estudiante.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * URL de la miniatura.
     *
     * @return string|null
     */
    public function getThumbnailUrlAttribute()
    {
        if ($this->thumbnail) {
            return route('beauties.files.show', [$this, $this->thumbnail]);
        }

        return null;
    }

    /**
     * URL de la foto original.
     *
     * @return string|null
     */
    public function getBeforeUrlAttribute()
    {
        if ($this->before) {
            return route('beauties.files.show', [$this, $this->before]);
        }

        return null;
    }

    /**
     * URL de la versión de Lightroom.
     *
     * @return string|null
     */
    public function getLightroomUrlAttribute()
    {
        if ($this->lightroom) {
            return route('beauties.files.show', [$this, $this->lightroom]);
        }

        return null;
    }

    /**
     * URL de la versión de Photoshop.
     *
     * @return string|null
     */
    public function getPhotoshopUrlAttribute()
    {
        if ($this->photoshop) {
            return route('beauties.files.show', [$this, $this->photoshop]);
        }

        return null;
    }

    /**
     * Crea o actualiza un beauty.
     *
     * @param StoreBeauty $request
     * @param User        $user
     * @param Beauty|null $beauty
     *
     * @return array
     * @throws \Throwable
     */
    public static function store(StoreBeauty $request, User $user, self $beauty = null)
    {
        DB::beginTransaction();
        try {
            // Update
            if ($beauty) {
                $msg = 'editó';
                $update = true;
            } // Insert
            else {
                $beauty = new self;
                $msg = 'creó';
                $update = false;
            }

            // Fotos
            $uploadedFiles = uploadFiles($beauty, $beauty->uploadFiles, $request, $update);

            $user->beauties()->save($beauty);

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El beauty se ' . $msg . ' correctamente — <a class="text-blue-600 hover:underline" href="' . route('beauties.index', $user) . '">ver beauties</a>.',
                'data'    => $beauty,
            ];
        } catch (\Exception $e) {
            deleteFilesFromDb($beauty, $uploadedFiles);
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Reordena los beauties.
     *
     * @param SortBeauty $request
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public static function order(SortBeauty $request)
    {
        DB::beginTransaction();
        try {
            $beauties = $request->input('beauties', []);

            if (count($beauties)) {
                foreach ($beauties as $i => $id) {
                    $beauty = self::findOrFail($id);
                    $beauty->sort = $i;
                    $beauty->save();
                }
            }

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'Los beauties se ordenaron correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Elimina un beauty.
     *
     * @return array|string[]
     * @throws \Throwable
     */
    public function eliminar()
    {
        DB::beginTransaction();
        try {
            // Elimina el beauty
            $this->delete();

            // Elimina los archivos asociados
            deleteFilesFromDb($this, $this->uploadFiles);

            // Commit
            DB::commit();

            return [
                'status'  => 'success',
                'message' => 'El beauty se eliminó correctamente.',
            ];
        } catch (\Exception $e) {
            DB::rollBack();

            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ];
        }
    }
}
