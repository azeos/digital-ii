<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeautiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beauties', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->char('thumbnail', 39)->unique()->nullable();
            $table->char('before', 39)->unique()->nullable();
            $table->char('lightroom', 39)->unique()->nullable();
            $table->char('photoshop', 39)->unique()->nullable();
            $table->unsignedBigInteger('sort')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beauties');
    }
}
