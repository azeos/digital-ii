<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('course_id')->nullable();
            $table->string('provider');
            $table->string('provider_id');
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->char('avatar', 39)->unique()->nullable();
            $table->char('header', 39)->unique()->nullable();
            $table->char('products_image', 39)->unique()->nullable();
            $table->char('photos_image', 39)->unique()->nullable();
            $table->char('beauties_image', 39)->unique()->nullable();
            $table->string('password')->nullable();
            $table->json('config')->nullable();
            $table->rememberToken();

            $table->timestamps();

            $table->unique(['provider', 'provider_id']);
            $table->unique(['id', 'course_id']);
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
