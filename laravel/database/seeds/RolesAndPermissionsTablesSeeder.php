<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Resetea el cache de roles y permisos
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Courses
        Permission::create(['name' => 'courses.viewAny']);
        Permission::create(['name' => 'courses.view']);
        Permission::create(['name' => 'courses.create']);
        Permission::create(['name' => 'courses.createAny']);
        Permission::create(['name' => 'courses.update']);
        Permission::create(['name' => 'courses.updateAny']);
        Permission::create(['name' => 'courses.delete']);
        Permission::create(['name' => 'courses.deleteAny']);

        // Users
        Permission::create(['name' => 'users.viewAny']);
        Permission::create(['name' => 'users.view']);
        Permission::create(['name' => 'users.create']);
        Permission::create(['name' => 'users.createAny']);
        Permission::create(['name' => 'users.update']);
        Permission::create(['name' => 'users.updateAny']);
        Permission::create(['name' => 'users.delete']);
        Permission::create(['name' => 'users.deleteAny']);

        // Products
        Permission::create(['name' => 'products.viewAny']);
        Permission::create(['name' => 'products.view']);
        Permission::create(['name' => 'products.create']);
        Permission::create(['name' => 'products.createAny']);
        Permission::create(['name' => 'products.update']);
        Permission::create(['name' => 'products.updateAny']);
        Permission::create(['name' => 'products.delete']);
        Permission::create(['name' => 'products.deleteAny']);

        // Photos
        Permission::create(['name' => 'photos.viewAny']);
        Permission::create(['name' => 'photos.view']);
        Permission::create(['name' => 'photos.create']);
        Permission::create(['name' => 'photos.createAny']);
        Permission::create(['name' => 'photos.update']);
        Permission::create(['name' => 'photos.updateAny']);
        Permission::create(['name' => 'photos.delete']);
        Permission::create(['name' => 'photos.deleteAny']);

        // Beauties
        Permission::create(['name' => 'beauties.viewAny']);
        Permission::create(['name' => 'beauties.view']);
        Permission::create(['name' => 'beauties.create']);
        Permission::create(['name' => 'beauties.createAny']);
        Permission::create(['name' => 'beauties.update']);
        Permission::create(['name' => 'beauties.updateAny']);
        Permission::create(['name' => 'beauties.delete']);
        Permission::create(['name' => 'beauties.deleteAny']);

        // Student
        Role::create(['name' => 'student'])->givePermissionTo([
            'courses.viewAny',
            'users.viewAny',
            'users.view',
            'users.update',
            'products.viewAny',
            'products.view',
            'products.create',
            'products.update',
            'products.delete',
            'photos.viewAny',
            'photos.view',
            'photos.create',
            'photos.update',
            'photos.delete',
            'beauties.viewAny',
            'beauties.view',
            'beauties.create',
            'beauties.update',
            'beauties.delete',
        ]);

        // Teacher
        Role::create(['name' => 'teacher'])->givePermissionTo([
            'courses.viewAny',
            'users.viewAny',
            'users.view',
            'users.create',
            'users.createAny',
            'users.update',
            'users.updateAny',
            'products.viewAny',
            'products.view',
            'products.create',
            'products.createAny',
            'products.update',
            'products.updateAny',
            'products.delete',
            'products.deleteAny',
            'photos.viewAny',
            'photos.view',
            'photos.create',
            'photos.createAny',
            'photos.update',
            'photos.updateAny',
            'photos.delete',
            'photos.deleteAny',
            'beauties.viewAny',
            'beauties.view',
            'beauties.create',
            'beauties.createAny',
            'beauties.update',
            'beauties.updateAny',
            'beauties.delete',
            'beauties.deleteAny',
        ]);

        // Admin
        Role::create(['name' => 'admin'])->givePermissionTo(Permission::all());
    }
}
