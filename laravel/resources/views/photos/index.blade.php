@extends('layouts.app')

@section('content')
    <photos-show
        :user="{{ $user->toJson() }}"
        :photos="{{ $photos->toJson() }}"
    ></photos-show>
@endsection
