@extends('layouts.app')

@section('content')
    <users-show :user="{{ $user->toJson() }}"></users-show>
@endsection
