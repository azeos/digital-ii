@extends('layouts.app')

@section('content')
    <user-settings
        :data="{{ $data }}"
        :profile="{{ $profile }}"
        section="{{ $section }}"
        :routes="{{ $routes }}"
    ></user-settings>
@endsection
