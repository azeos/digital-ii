@extends('layouts.app')

@section('content')
    <div class="container mx-auto pb-8">
        <div class="flex flex-wrap justify-center">
            <div class="w-full max-w-sm px-4">
                <div class="flex flex-col mt-8 break-words bg-white border border-2 rounded shadow-md">

                    <div class="font-semibold bg-gray-200 text-gray-700 py-3 px-6">
                        Login
                    </div>

                    <div class="w-full p-6 text-center">
                        <p class="mb-4 text-center">Utilizar cuenta @eaf.edu.ar</p>
                        <a class="inline-block px-4 bg-red-600 hover:bg-red-700 text-white font-bold rounded focus:outline-none focus:shadow-outline" href="{{ route('login.provider', 'google') }}">
                            <span class="inline-block py-2 pr-4 mr-4 border-r">
                                <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    class="inline-block h-4"
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 488 512"
                                >
                                    <path fill="currentColor" d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"></path>
                                </svg>
                            </span>
                            Ingresar
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
