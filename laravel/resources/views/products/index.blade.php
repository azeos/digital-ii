@extends('layouts.app')

@section('content')
    <products-show
        :user="{{ $user->toJson() }}"
        :products="{{ $products->toJson() }}"
    ></products-show>
@endsection
