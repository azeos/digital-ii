@extends('layouts.app')

@section('content')
    <courses-index :courses="{{ $courses->toJson() }}"></courses-index>
@endsection
