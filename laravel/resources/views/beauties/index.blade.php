@extends('layouts.app')

@section('content')
    <beauties-show
        :user="{{ $user->toJson() }}"
        :beauties="{{ $beauties->toJson() }}"
    ></beauties-show>
@endsection
