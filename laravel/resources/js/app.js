/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import VueTailwind from 'vue-tailwind';
import Theme from './app/components/theme.js';
import VueHolder from 'vue-holderjs';
import VueCompareImage from 'vue-compare-image';

Vue.use(VueTailwind, Theme);
Vue.use(VueHolder);
Vue.component('VueCompareImage', VueCompareImage);

Vue.component('main-header', require('./app/components/MainHeader.vue').default);
Vue.component('sections-nav', require('./app/components/SectionsNav.vue').default);

Vue.component('courses-index', require('./app/components/CoursesIndex.vue').default);

Vue.component('user-settings', require('./app/components/UserSettings.vue').default);

Vue.component('users-create', require('./app/components/UsersCreate.vue').default);
Vue.component('users-show', require('./app/components/UsersShow.vue').default);

Vue.component('photos-create', require('./app/components/PhotosCreate.vue').default);
Vue.component('photos-show', require('./app/components/PhotosShow.vue').default);

Vue.component('products-create', require('./app/components/ProductsCreate.vue').default);
Vue.component('products-show', require('./app/components/ProductsShow.vue').default);

Vue.component('beauties-create', require('./app/components/BeautiesCreate.vue').default);
Vue.component('beauties-show', require('./app/components/BeautiesShow.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    mounted() {
        // autofocus Fx fix https://github.com/vuejs/vue/issues/8112
        const input = document.querySelector('[autofocus]');
        if (input) {
            input.focus();
        }
    },
});
