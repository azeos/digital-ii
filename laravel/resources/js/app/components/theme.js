const Theme = {
    TInput: {
        fixedClasses: 'form-input',
        classes: 'block w-full disabled:bg-gray-300',
        variants: {
            danger: 'block w-full border-red-300 bg-red-100'
        }
    },
    TTextarea: {
        fixedClasses: 'form-textarea',
        classes: 'block w-full',
        variants: {
            danger: 'block w-full border-red-300 bg-red-100'
        }
    },
    TSelect: {
        fixedClasses: 'form-select',
        classes: 'block w-full',
        variants: {
            danger: 'block w-full border-red-300 bg-red-100'
        }
    },
    TRadio: {
        fixedClasses: 'form-radio transition duration-150 ease-in-out',
        classes: '',
        variants: {
            error: 'text-red-500 border-red-500 bg-red-100',
            success: 'text-green-500 border-green-500 bg-green-100'
        }
    },
    TCheckbox: {
        fixedClasses: 'form-checkbox transition duration-150 ease-in-out',
        classes: '',
        variants: {
            error: 'text-red-500 border-red-500 bg-red-100',
            success: 'text-green-500 border-green-500 bg-green-100'
        }
    },
    TInputGroup: {
        fixedClasses: {
            wrapper: 'mb-4',
            label: 'block uppercase tracking-wide text-xs font-bold mb-1',
            body: '',
            feedback: 'text-sm',
            description: 'text-sm'
        },
        classes: {
            wrapper: '',
            label: '',
            body: '',
            feedback: 'text-gray-500',
            description: 'text-gray-500'
        },
        variants: {
            danger: {
                label: 'text-red-500',
                feedback: 'text-red-500'
            }
        }
    },
    TAlert: {
        fixedClasses: {
            wrapper: 'rounded p-4 flex text-sm border-l-4',
            body: 'flex-grow',
            close: 'ml-4 rounded',
            closeIcon: 'h-5 w-5 fill-current'
        },
        classes: {
            wrapper: 'bg-blue-100 border-blue-500',
            body: 'text-blue-700',
            close: 'text-blue-700 hover:text-blue-500 hover:bg-blue-200',
            closeIcon: 'h-5 w-5 fill-current'
        },
        variants: {
            danger: {
                wrapper: 'bg-red-100 border-red-500',
                body: 'text-red-700',
                close: 'text-red-700 hover:text-red-500 hover:bg-red-200'
            },
            success: {
                wrapper: 'bg-green-100 border-green-500',
                body: 'text-green-700',
                close: 'text-green-700  hover:text-green-500 hover:bg-green-200'
            }
        }
    },
    TModal: {
        fixedClasses: {
            overlay: 'z-40 overflow-auto inset-0 w-full h-full fixed bg-opacity-50',
            wrapper: 'z-50 relative mx-auto my-0',
            modal: 'overflow-hidden relative',
            body: 'p-4',
            header: 'p-4 border-b text-sm font-semibold uppercase text-gray-700',
            footer: 'p-4 border-t',
            close: 'right-0 top-0',
            closeIcon: 'h-5 w-5 fill-current'
        },
        classes: {
            overlay: 'bg-black',
            wrapper: 'max-w-3xl p-3 mt-12',
            modal: 'bg-white shadow',
            body: '',
            header: '',
            footer: '',
            close: 'absolute m-3 text-gray-700 hover:text-gray-600',
            closeIcon: 'h-5 w-5 fill-current',
            overlayEnterClass: '',
            overlayEnterActiveClass: 'opacity-0 transition ease-out duration-300',
            overlayEnterToClass: 'opacity-100',
            overlayLeaveClass: 'transition ease-in opacity-100',
            overlayLeaveActiveClass: '',
            overlayLeaveToClass: 'opacity-0 duration-75',
            enterClass: '',
            enterActiveClass: '',
            enterToClass: '',
            leaveClass: '',
            leaveActiveClass: '',
            leaveToClass: '',
        },
        variants: {
            picture: {
                wrapper: 'z-50 table h-full',
                modal: 'table-cell align-middle',
                close: 'picture-close fixed m-6 text-white transform scale-150',
            },
            danger: {
                overlay: 'bg-red-500',
                header: 'border-red-100 text-red-700',
                footer: 'border-red-100 bg-red-100',
                close: 'text-red-700 hover:text-red-600'
            }
        }
    },
    TDropdown: {
        classes: {
            button: 'px-4 py-2 flex items-center border rounded hover:text-gray-700',
            wrapper: 'inline-flex flex-col',
            dropdownWrapper: 'relative z-10',
            dropdown: 'origin-top-right absolute right-0 w-56 rounded-md shadow-lg bg-white',
            enterClass: '',
            enterActiveClass: 'transition ease-out duration-100 transform opacity-0 scale-95',
            enterToClass: 'transform opacity-100 scale-100',
            leaveClass: 'transition ease-in transform opacity-100 scale-100',
            leaveActiveClass: '',
            leaveToClass: 'transform opacity-0 scale-95 duration-75'
        },
        variants: {
            navbarDropdown: {
                button: 'hover:underline text-gray-300 text-sm p-3',
                dropdown: 'origin-top-right absolute right-0 rounded-md shadow bg-white'
            }
        }
    }
}

export default Theme
