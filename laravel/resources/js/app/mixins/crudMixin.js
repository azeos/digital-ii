import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner, faCameraAlt, faTrashAlt, faArrows } from '@fortawesome/pro-light-svg-icons';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faSpinner, faCameraAlt, faTrashAlt, faArrows, faAngleDown);

export const crudMixin = {
    components: {
        'font-awesome-icon': FontAwesomeIcon,
    },

    data() {
        return {
            showPageInfo: false,    // Indica si se despliega el alert con la información del formulario
            showModalInfo: false,   // Indica si se despliega el alert del modal con la información del formulario
            alerts: [],             // Alerts con los mensajes de los formularios
            formObject: {},         // Objeto que contiene la información del formulario
            formLoading: false,     // Loading del formModal
            formSending: false,     // Loading en el botón de guardar
            formDeleting: false,    // Loading en el botón de eliminar
            formErrors: {           // Errores del formulario
                message: null,
                errors: [],
            },
        }
    },

    methods: {
        /**
         * Abre el modal para editar o crear un modelo.
         *
         * @param {string} url URL desde la que se trae el formulario.
         */
        editModel(url) {
            this.formLoading = true;
            this.$modal.show('formModal')

            axios.get(url).then(({data}) => {
                // Carga los campos
                this.formObject = data;
            }).catch((error) => {
                this.formErrors.errors = [];
                this.formErrors.message = null;

                // Recorre los errores para determinar si se
                // muestran los individuales o el general.
                if (error.response.data.errors) {
                    this.formErrors.errors = error.response.data.errors;
                } else if (error.response.data.message) {
                    this.formErrors.message = error.response.data.message;
                } else if (typeof error.response.data == 'string') {
                    this.formErrors.message = error.response.data;
                } else {
                    this.formErrors.message = error.response.statusText;
                }
            }).finally(() => {
                this.formLoading = false;
            });
        },

        /**
         * Guarda, o elimina, un modelo por AJAX.
         *
         * @param {element} form Formulario que se va a enviar.
         * @param {boolean} destroy Indica si la acción es un delete.
         */
        updateModel(form, destroy = false) {
            // Button loading
            if (destroy) {
                this.formDeleting = true;
            } else {
                this.formSending = true;
            }

            // Form data
            let formData = new FormData(form);

            // Method
            let method = destroy ? 'delete' : 'post';

            // AJAX
            axios({
                method: method,
                url: form.action,
                data: formData,
            }).then(({data}) => {
                // Cierra el modal
                this.$modal.hide('formModal');

                // Actualiza la información
                this.fetchedData = data.data;

                // Muestra el mensaje
                this.$emit('modelUpdatedSuccess', data.message);
                this.formErrors.errors = [];
                this.formErrors.message = null;

                // Scroll top para que se vea el mensaje
                window.scrollTo(0, 0);
            }).catch((error) => {
                this.formSending = false;
                this.formDeleting = false;
                this.formErrors.errors = [];
                this.formErrors.message = null;

                // Recorre los errores para determinar si se
                // muestran los individuales o el general.
                if (error.response.data.errors) {
                    this.formErrors.errors = error.response.data.errors;
                } else if (error.response.data.message) {
                    this.formErrors.message = error.response.data.message;
                } else if (typeof error.response.data == 'string') {
                    this.formErrors.message = error.response.data;
                } else {
                    this.formErrors.message = error.response.statusText;
                }
            }).finally(() => {
                this.formSending = false;
                this.formDeleting = false;
            });
        },

        /**
         * Borra el contenido del formulario.
         */
        clearForm() {
            this.showModalInfo = false;
            this.formObject = {};
            this.formSending = false;
            this.formDeleting = false;
            this.formErrors = {message: null, errors: []};
        },

        /**
         * Devuelve los errores de un array devuelto por Laravel
         *
         * @param {object} errors
         * @param {string} inputKey
         * @return {[]}
         */
        getArrayErrors(errors, inputKey) {
            let messages = [];
            // Recorre todas las entradas de errores y revisa si
            // no hay alguna que empiece con la key del input
            _.forEach(Object.keys(errors), (key) => {
                if (_.startsWith(key, inputKey + '.') || key == inputKey) {
                    _.forEach(errors[key], (value) => {
                        messages.push(value);
                    });
                }
            });

            return messages;
        },

        /**
         * Muestra el preview de la imagen a subir.
         *
         * @param {event} e Evento del input.
         * @param {string} img Nombre del objeto que contiene la imagen.
         */
        imagePreview(e, img) {
            let file = e.target.files[0];
            this[img].url = URL.createObjectURL(file);
            this[img].delete = false;
        },

        /**
         * Borra una imagen.
         *
         * @param {string} img Nombre del objeto que contiene la imagen.
         */
        imageDelete(img) {
            this[img].url = null;
            this[img].delete = true;
            this.$refs[img + 'Input'].value = '';
        },

        /**
         * Permite hacer un drag & drop sobre las imágenes.
         *
         * @param {event} e Evento del drag & drop.
         * @param {string} img Nombre del objeto que contiene la imagen.
         */
        imageDrop(e, img) {
            this.$refs[img + 'Input'].files = e.dataTransfer.files;
            let change = new Event('change');
            this.$refs[img + 'Input'].dispatchEvent(change);
        },
    }
}
