<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Auth::routes(['register' => false, 'reset' => false, 'confirm' => false, 'verify' => false]);
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

// App
Route::middleware('auth')->group(function () {
    // Home
    Route::get('/', 'CourseController@index')->name('index');

    // Archivos
    Route::resource('beauties.files', 'FileController')->only(['show']);
    Route::resource('photos.files', 'FileController')->only(['show']);
    Route::resource('pictures.files', 'FileController')->only(['show']);
    Route::resource('products.files', 'FileController')->only(['show']);

    // Prefijo con el slug del usuario
    Route::prefix('{user}')->group(function () {
        // Users
        Route::get('/', 'UserController@show')->name('users.show');
        Route::get('edit', 'UserController@edit')->name('users.edit');
        Route::patch('/', 'UserController@update')->name('users.update');
        Route::get('files/{file}', 'FileController@show')->name('users.files.show');

        // Products
        Route::post('products/sort', 'ProductController@sort')->name('products.sort');
        Route::resource('products', 'ProductController');

        // Photos
        Route::post('photos/sort', 'PhotoController@sort')->name('photos.sort');
        Route::resource('photos', 'PhotoController');

        // Beauties
        Route::post('beauties/sort', 'BeautyController@sort')->name('beauties.sort');
        Route::resource('beauties', 'BeautyController');
    });
});
